-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2016 at 05:12 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `course_manger`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `created`, `updated`, `deleted`) VALUES
(1, '1321321', 'php', '5', 'sdasd', 'web application', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'asdfasd', 'asp.net', '', '', 'web application', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE IF NOT EXISTS `course_trainer_lab_mapping` (
`id` int(11) NOT NULL,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` int(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `is_running` int(11) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `course_trainer_lab_mapping`
--

INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`) VALUES
(1, 22, 'asd', 'sad', 'sad', 'asdf', 0, 'adf', 'asdf', '0000-00-00', 'dsf', 'adsf', 255, 'ads', '2016-08-18 00:00:00', '2016-08-18 00:00:00', '2016-08-26 00:00:00'),
(2, 3, '3', 'dd', 'd', 'd', 0, 'd', 'd', '2016-08-02', 'd', 'd', 0, 'd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, '', '', '', '', 0, '', '', '0000-00-00', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3, '', '', '', '', 0, '', '', '0000-00-00', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 3, '1', 'd', 'd', 'd', 2, '33', '33', '2016-08-02', '3', '3', 3, 'd', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(6, 3, '1', 'd', 'd', 'd', 2, '33', '33', '2016-08-02', '3', '3', 3, 'd', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(7, 3, '1', 'd', 'd', 'd', 2, '33', '33', '2016-08-02', '3', '3', 3, 'd', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(8, 3, '1', 'd', 'd', 'd', 2, '33', '33', '2016-08-02', '3', '3', 3, 'd', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(9, 3, '1', 'd', 'd', 'd', 2, '33', '33', '2016-08-02', '3', '3', 3, 'd', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(10, 0, '', '', '', '', 0, '', '', '', '', '', 0, '', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(11, 0, '', '', '', '', 0, '', '', '', '', '', 0, '', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(12, 0, '', '', '', '', 0, '', '', '', '', '', 0, 'tuhin', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(13, 0, '', '', '', '', 0, '', '', '', '', '', 2, 'tuhin', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(14, 12, '10', 'linux', 'window', 'window', 1, 'Wed Aug 17 2016', '', '10', 'd', 'Sun,Tue,Thu', 2, 'tuhin', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(15, 1, '10', '1', '1', '1', 14, 'Mon Aug 29 2016', '', '60', '50', 'Sat,Mon,Wed', 2, 'tuhin', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00'),
(16, 1, '', '1', '1', '1', 1, 'Tue Aug 30 2016', '', '50', '30', 'Sat,Mon,Wed', 2, 'tuhin', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE IF NOT EXISTS `installed_softwares` (
  `id` int(11) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE IF NOT EXISTS `labinfo` (
`id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lab_no` varchar(255) NOT NULL,
  `seat_capacity` varchar(255) NOT NULL,
  `projector_resolution` varchar(255) NOT NULL,
  `ac_status` varchar(255) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc` varchar(255) NOT NULL,
  `table_capacity` varchar(255) NOT NULL,
  `internet_speed` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc`, `table_capacity`, `internet_speed`) VALUES
(1, 10, 'dd', 'd', 'd', 'yes', 'dd', 'linux', 'd', 'd', 'd'),
(2, 10, '', '', '', '', '', 'window', '', '', ''),
(3, 10, 'ddd', 'd', '', '', '', 'window', '', '', ''),
(4, 10, 'asd', '', '', '', '', 'window', '', '', ''),
(5, 10, 'asd', 'd', 'asdf', '', '', 'window', '', '', ''),
(6, 10, 'asd', 'd', 'asdf', '', '', 'window', '', '', ''),
(7, 10, '', '', '', '', '', 'window', '', '', ''),
(8, 10, 'asd', 'd', 'asdf', 'yes', '''''', 'window', 'value="', '', ''),
(9, 10, '', '', '', '', '', 'window', 'kjhkkj', '', ''),
(10, 10, 'd', '', '', '', '', 'window', '', '', ''),
(11, 10, 'd', '', '', '', '', 'window', '', '', ''),
(12, 10, 'd', '', '', '', '', 'window', 'jjj', '', ''),
(13, 10, 'dd', 'd', 'd', 'yes', 'd', 'linux', 'jjjdd', 'dd', 'dd'),
(14, 10, 'ddd', '', '', '', '', 'window', 'jjjdd', '', ''),
(15, 10, 'd', 'd', 'd', 'yes', '', 'window', 'dd', '', ''),
(16, 10, 'd', '', '', '', '', 'window', '', '', ''),
(17, 10, '', '', '', '', '', 'window', '', '', ''),
(18, 10, 'd', '', '', '', '', 'window', '', '', ''),
(19, 10, 'd', '', '', '', '', 'window', '', '', ''),
(20, 10, 'd', '', '', '', '', 'window', 'd', '', ''),
(21, 10, 'd', '', '', '', '', 'window', '', '', ''),
(22, 10, '', '', '', '', '', 'window', '', '', ''),
(23, 10, '', '', '', '', '', 'window', '', '', ''),
(24, 0, '', '', '', '', '', 'window', '', '', ''),
(25, 10, 'd', '', '', '', '', 'window', '', '', ''),
(26, 10, '', '', '', 'Empty', '', 'window', '', '', ''),
(27, 10, '', '', '', 'Empty', '', 'window', '', '', ''),
(28, 10, 'd', '', '', 'Empty', 'd', 'window', 'd', '', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE IF NOT EXISTS `trainer` (
  `id` int(11) NOT NULL,
  `unique_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(255) NOT NULL,
  `Course_id` int(11) NOT NULL,
  `trainer_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adddress` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `web_address` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `Course_id`, `trainer_status`, `image`, `phone`, `email`, `adddress`, `gender`, `web_address`, `created`, `modified`, `deleted`) VALUES
(1, 2222, 'tuhin', 'bsc in cse', 'php', 1, 'lead_trainer', 'nai', '0191111', 'ta@gmail.com', '537,dania', 'male', 'www.nai.com', '2016-08-17 00:00:00', '2016-08-17 00:00:00', '2016-08-30 00:00:00'),
(2, 1, '', '', 'asp.net', 2, 'lab_asst', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, '', '', 'web_design', 3, 'asst_trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2222, 'tuhin', 'bsc in cse', 'php', 1, 'asst_trainer', 'nai', '0191111', 'ta@gmail.com', '537,dania', 'male', 'www.nai.com', '2016-08-17 00:00:00', '2016-08-17 00:00:00', '2016-08-30 00:00:00'),
(5, 1, '', '', 'asp.net', 1, 'lab_asst', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

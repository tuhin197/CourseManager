
<?php
session_start();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['success_mess'];
    unset($_SESSION['success_mess']);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CodeBees Course Management</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>

        <script type="text/javascript" src="../assets/js/core/app.js"></script>
        <script type="text/javascript" src="../assets/js/pages/dashboard.js"></script>
        <!-- /theme JS files -->
        
                <script>
                    var carsAndModels = {};
                    carsAndModels['yes'] = ['1', '2', '3'];


                    function ChangeCarList() {
                        var carList = document.getElementById("ac_status");
                        var modelList = document.getElementById("total_ac");
                        var selCar = carList.options[carList.selectedIndex].value;
                        while (modelList.options.length) {
                            modelList.remove(0);
                        }
                        var cars = carsAndModels[selCar];
                        if (cars) {
                            var i;
                            for (i = 0; i < cars.length; i++) {
                                var ac_status = new Option(cars[i], i);
                                modelList.options.add(ac_status);
                            }
                        }
                    }
                </script>

    </head>

    <body>

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="../assets/images/logo_light.png" alt=""></a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>


                </ul>



                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown language-switch">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/flags/gb.png" class="position-left" alt="">
                            English
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a class="deutsch"><img src="../assets/images/flags/de.png" alt=""> Deutsch</a></li>
                            <li><a class="ukrainian"><img src="../assets/images/flags/ua.png" alt=""> Українська</a></li>
                            <li><a class="english"><img src="../assets/images/flags/gb.png" alt=""> English</a></li>
                            <li><a class="espana"><img src="../assets/images/flags/es.png" alt=""> España</a></li>
                            <li><a class="russian"><img src="../assets/images/flags/ru.png" alt=""> Русский</a></li>
                        </ul>
                    </li>



                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/placeholder.jpg" alt="">
                            <span>Victoria</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                            <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                            <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                            <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="../assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Victoria Baker</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <!-- Main -->
                                    <li class="navigation-header"><span>Main tuhin</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li><a href="../index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                    <li>
                                        <a href="#"><i class="icon-stack2"></i> <span>User Mangment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-copy"></i> <span>Trainer Mangment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-certificate"></i> <span>Course Information</span></a>

                                    </li>
                                    <li class="active">
                                        <a href="#"><i class="icon-lab"></i> <span>Lab Information</span></a>
                                        <ul>
                                            <li><a href="add-new-lab.php" id="layout2">Add new lab</a></li>
                                            <li><a href="alllabinfo.php" id="layout3">All Lab Information</a></li>

                                        </ul>

                                    </li>
                                    <li><a href="#"><i class="icon-code"></i> <span>Software Managment</span></a></li>
                                    <li><a href="../AssignCourse/course_assign.php"><i class="icon-link"></i> <span>Assign Course</span></a></li>


                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard </h4>

                            </div>

                            <div class="heading-elements">
                                <div class="heading-btn-group">
                                    <a href="alllabinfo.php" class="btn btn-link btn-float has-text"><i class="icon-eye"></i><span>All Lab Information</span></a>
                                    <a href="assigncourse.php" class="btn btn-link btn-float has-text"><i class="icon-certificate"></i> <span>Assign New Course</span></a>
                                    <a href="search.php" class="btn btn-link btn-float has-text"><i class="icon-search4"></i> <span>Search Lab Info</span></a>


                                </div>
                            </div>
                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li>Lab Information</li>
                                <li><a href="add-new-lab.php">Add New Lab</a></li>

                            </ul>

                            <ul class="breadcrumb-elements">
                                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-gear position-left"></i>
                                        Settings
                                        <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            
                            <div class="panel-body">

                                <form class="form-horizontal form-validate-jquery" action="store.php" method="post">
                                    <fieldset class="content-group">
                                        <legend class="text-bold">Add Lab Informations</legend>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <label class="control-label col-md-3">Lab No:</label>
                                                    <div class="col-md-9 <?php
                                                    if (isset($_SESSION['lab_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['lab_requir']);
                                                    }
                                                    ?>">
                                                        <input type="text" class="form-control" name="lab_no" placeholder="Enter Lab Nomber...." value="<?php
                                                        if (isset($_SESSION['requir_data']['lab_no'])) {
                                                            echo $_SESSION['requir_data']['lab_no'];
                                                            unset($_SESSION['requir_data']['lab_no']);
                                                        }
                                                        ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Course ID:</label>
                                                    <div class="col-md-9 <?php
                                                    if (isset($_SESSION['lab_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['lab_requir']);
                                                    }
                                                    ?>">
                                                        <select class="form-control" id="job" name="course_id">
                                                            <option value="">Select Course</option>
                                                            <optgroup label="Web Application">
                                                                <option value="1">Front-End Development HTML, CSS</option>
                                                                <option value="2">Web Application PHP</option>
                                                                <option value="3">Python Development</option>
                                                                <option value="4"> Rails Development</option>
                                                                <option value="5">Web Designing</option>
                                                                <option value="6">WordPress Development</option>
                                                            </optgroup>
                                                            

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Seat Capacity:</label>
                                                    <div class="col-md-9  <?php
                                                    if (isset($_SESSION['lab_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['lab_requir']);
                                                    }
                                                    ?>">
                                                        <input type="text" class="form-control" placeholder="Enter Seat Capacity..." name="seat_capacity">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 <?php
                                                    if (isset($_SESSION['lab_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['lab_requir']);
                                                    }
                                                    ?>">Projector Resolution:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="1200x800" name="projector_resolution">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 <?php
                                                    if (isset($_SESSION['lab_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['lab_requir']);
                                                    }
                                                    ?>">AC Status:</label>
                                                    <div class="col-md-7">
                                                        <select id="ac_status" name="" onchange="ChangeCarList()" class="form-control">
                                                            <option value="">Select AC Status</option>
                                                            <option value="yes">Yes</option>
                                                            <option value="no">NO</option>

                                                        </select>                                                            
                                                    </div>
                                                    <div class="col-md-2">

                                                        <select id="total_ac" name="ac_status"  class="form-control">
                                                            <option>Empty</option>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                    if (isset($_SESSION['lab_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['lab_requir']);
                                                    }
                                                    ?>">
                                                    <label class="control-label col-md-3 ">Operating System:</label>
                                                    <div class="col-md-9">
                                                        <select id="os" onchange="ChangeCarList()" class="form-control" name="os">
                                                            <option value="window">windows</option>
                                                            <option value="linux">Linux</option>
                                                            <option value="mac">Mac</option>


                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                    if (isset($_SESSION['pc_requir'])) {
                                                        echo 'has-warning';
                                                        unset($_SESSION['pc_requir']);
                                                    }
                                                    ?>">
                                                    <label class="control-label col-md-3 ">PC Configuration:</label>
                                                    <div class="col-md-9">
                                                        <textarea rows="5" cols="5" class="form-control" name="pc_configuration" placeholder="Enter pc_configuration"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (isset($_SESSION['trainer_pc_requir'])) {
                                                    echo 'has-warning';
                                                    unset($_SESSION['trainer_pc_requir']);
                                                }
                                                ?>">
                                                    <label class="control-label col-md-3">Trainer PC Configuration:</label>
                                                    <div class="col-md-9">
                                                        <textarea rows="5" cols="5" class="form-control" name="trainer_pc" placeholder="Enter trainer_pc_configuration"><?php
                                                            if (isset($_SESSION['requir_data']['trainer_pc'])) {
                                                                echo $_SESSION['requir_data']['trainer_pc'];
                                                                unset($_SESSION['requir_data']['trainer_pc']);
                                                            }
                                                            ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Table Capacity:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="table_capacity" placeholder="Enter table_capacity">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                    if (isset( $_SESSION['internet_requir'])) {
                                                        echo 'has-warning';
                                                        unset( $_SESSION['internet_requir']);
                                                    }
                                                    ?>">
                                                    <label class="control-label col-md-3">Internet Speed:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="internet_speed" placeholder="Enter internet_speed...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    

                                    </fieldset>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Add Lab Info<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </form>	

                            </div>
                            <!-- /basic datatable -->






                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2015. <a href="#">Tuhin</a> by <a href="#" target="_blank">Nai</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

    </body>
</html>

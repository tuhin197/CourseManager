<?php 
include_once("../../vendor/autoload.php");
use App\Labinfo\Labinfo;
$obj = new Labinfo();

$viewData = $obj->prepare($_GET)->show();
include 'header.php';
?>
<!DOCTYPE html>
<html lang="en">



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
							
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li>Dashboard</li>
							
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
                                                    <h5 class="panel-title">Basic datatable</h5>
                                                  <div class="heading-elements">
                                                    <ul class="icons-list">
                                                            <li><a data-action="collapse"></a></li>
                                                            <li><a data-action="reload"></a></li>
                                                            <li><a data-action="close"></a></li>
                                                    </ul>
                                                  </div>
						</div>

						<div class="panel-body">
							The <code>DataTables</code> is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function. Searching, ordering, paging etc goodness will be immediately added to the table, as shown in this example. <strong>Datatables support all available table styling.</strong>
						</div>

						<table class="table-bordered table-lg">
							
							<tbody>
                                                            <tr>
                                                                    <th>ID</th>
                                                                    <td><?php echo $viewData['id']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                    <th>course ID</th>
                                                                    <td><?php echo $viewData['course_id']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                   <th>Lab NO:</th>
                                                                   <td><?php echo $viewData['lab_no']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                    <th>Seat Capacity</th>
                                                                    <td><?php  echo $viewData['seat_capacity']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                   <th>Projector Resulation</th>
                                                                    <td><?php  echo $viewData['projector_resolution']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                    <th>Ac Status</th>
                                                                    <td><?php  echo $viewData['ac_status']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                    <th>Pc Configuarion</th>
                                                                    <td><?php  echo $viewData['pc_configuration']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                   <th>Opearating system</th>
                                                                    <td><?php  echo $viewData['os']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                   <th>trainer_pc_configuration</th>
                                                                    <td><?php  echo $viewData['trainer_pc']; ?></td>
                                                                </tr>	
                                                                <tr>
                                                                     <th>Table Capacity</th>
                                                                    <td><?php  echo $viewData['table_capacity']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Internet_speed</th>
                                                                    <td><?php  echo $viewData['internet_speed']; ?></td>
                                                                </tr>	
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->

					




					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Tuhin</a> by <a href="#" target="_blank">Nai</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

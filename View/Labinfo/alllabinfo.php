<?php
include_once("../../vendor/autoload.php");

use App\Labinfo\Labinfo;

$obj = new Labinfo();
$showData = $obj->index();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>themelock.com - Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="../assets/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>

        <script type="text/javascript" src="../assets/js/core/app.js"></script>
        <script type="text/javascript" src="../assets/js/pages/dashboard.js"></script>
        <!-- /theme JS files -->
        <script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
</script>

    </head>

    <body>

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="../assets/images/logo_light.png" alt=""></a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>


                </ul>



                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown language-switch">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/flags/gb.png" class="position-left" alt="">
                            English
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a class="deutsch"><img src="../assets/images/flags/de.png" alt=""> Deutsch</a></li>
                            <li><a class="ukrainian"><img src="../assets/images/flags/ua.png" alt=""> Українська</a></li>
                            <li><a class="english"><img src="../assets/images/flags/gb.png" alt=""> English</a></li>
                            <li><a class="espana"><img src="../assets/images/flags/es.png" alt=""> España</a></li>
                            <li><a class="russian"><img src="../assets/images/flags/ru.png" alt=""> Русский</a></li>
                        </ul>
                    </li>



                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/placeholder.jpg" alt="">
                            <span>Victoria</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                            <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                            <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                            <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="../assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Victoria Baker</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <!-- Main -->
                                    <li class="navigation-header"><span>Main tuhin</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li><a href="../index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                    <li>
                                        <a href="#"><i class="icon-stack2"></i> <span>User Mangment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-copy"></i> <span>Trainer Mangment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-certificate"></i> <span>Course Information</span></a>

                                    </li>
                                    <li class="active">
                                        <a href="#"><i class="icon-lab"></i> <span>Lab Information</span></a>
                                        <ul>
                                            <li><a href="add-new-lab.php" id="layout2">Add new lab</a></li>
                                            <li><a href="alllabinfo.php" id="layout3">All Lab Information</a></li>

                                        </ul>

                                    </li>
                                    <li><a href="#"><i class="icon-code"></i> <span>Software Managment</span></a></li>
                                    <li><a href="../AssignCourse/assigncourse.php"><i class="icon-link"></i> <span>Assign Course</span></a></li>


                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>

                            </div>

                            <div class="heading-elements">
                                <div class="heading-btn-group">
                                    <a href="add-new-lab.php" class="btn btn-link btn-float has-text"><i class="icon-lab"></i><span>Add New Lab</span></a>
                                    <a href="assigncourse.php" class="btn btn-link btn-float has-text"><i class="icon-certificate"></i> <span>Assign New Course</span></a>
                                    <a href="search.php" class="btn btn-link btn-float has-text"><i class="icon-search4"></i> <span>Search Lab Info</span></a>


                                </div>
                            </div>
                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li>Dashboard</li>

                            </ul>

                            <ul class="breadcrumb-elements">
                                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-gear position-left"></i>
                                        Settings
                                        <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">
 <li><form action="">
                                        First name: <input type="text" id="txt1" onkeyup="showHint(this.value)">
                                    </form>

                                    <p>Suggestions: <span id="txtHint"></span></p></li>
                            </ul>
                            <script>
                                function showHint(str) {
                                    var xhttp;
                                    if (str.length == 0) {
                                        document.getElementById("txtHint").innerHTML = "";
                                        return;
                                    }
                                    xhttp = new XMLHttpRequest();
                                    xhttp.onreadystatechange = function () {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                            document.getElementById("txtHint").innerHTML = xhttp.responseText;
                                        }
                                    };
                                    xhttp.open("GET", "livesearch.php?id=" + str, true);
                                    xhttp.send();
                                }
                            </script>
                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">All Lab Informations</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">

<table  id="myTable" class="table table-striped table-bordered" >   
                                
                                    <thead >

                                        <tr style="text-align: center; background: #26a69a; color:#fff">
                                            <th class="sorting" aria-controls="example" rowspan="1" aria-lable="SL activate to sort column ascending">SL</th>
                                            <th>Course title</th>
                                            <th>Lab NO:</th>
                                            <th>Seat Capacity</th>
                                            <th>Projector Resulation</th>
                                            <th colspan="3" class="text-center">Action</th>

                                        </tr>
                                    </thead>
                                    <tbody style="text-align: center">
                                        <?php
                                        $serial = 1;
                                        if (!empty($showData)) {
                                            foreach ($showData as $one) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $serial++; ?></td>
                                                    <td><?php echo $one['course_id'] ?></a></td>
                                                    <td><?php echo $one['lab_no'] ?></td>
                                                    <td><?php echo $one['seat_capacity'] ?></td>
                                                    <td><?php echo $one['projector_resolution'] ?></td>
                                                    <td><a class="btn btn-primary" href="view.php?id=<?php echo $one['id'] ?>" alt="View"> <i class="icon-eye4"></i></a></td>
                                                    <td><a  class="btn btn-success" href="edit.php?id=$one['id']"><i class="icon-pen"></i></a></td>
                                                    <td><a class="btn btn-danger" href="edit.php?id=$one['id']"><i class="icon-database-remove"></i></a></td>



                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            echo "No data";
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /basic datatable -->






                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2015. <a href="#">Tuhin</a> by <a href="#" target="_blank">Nai</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

    </body>
</html>

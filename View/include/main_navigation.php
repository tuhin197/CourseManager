 <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="../assets/images/logo_light.png" alt=""></a>
                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>


                </ul>



                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown language-switch">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/flags/gb.png" class="position-left" alt="">
                            English
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a class="deutsch"><img src="../assets/images/flags/de.png" alt=""> Deutsch</a></li>
                            <li><a class="ukrainian"><img src="../assets/images/flags/ua.png" alt=""> Українська</a></li>
                            <li><a class="english"><img src="../assets/images/flags/gb.png" alt=""> English</a></li>
                            <li><a class="espana"><img src="../assets/images/flags/es.png" alt=""> España</a></li>
                            <li><a class="russian"><img src="../assets/images/flags/ru.png" alt=""> Русский</a></li>
                        </ul>
                    </li>



                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/placeholder.jpg" alt="">
                            <span>Victoria</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                            <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                            <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                            <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="../assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Victoria Baker</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <!-- Main -->
                                    <li class="navigation-header"><span>Main tuhin</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li class="active"><a href="../index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                    <li>
                                        <a href="#"><i class="icon-stack2"></i> <span>User Mangment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-copy"></i> <span>Trainer Mangment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-droplet2"></i> <span>Course Information</span></a>

                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-list-unordered"></i> <span>Lab Information</span></a>
                                        <ul>
                                            <li><a href="add-new-lab.php" id="layout2">Add new lab</a></li>
                                            <li><a href="alllabinfo.php" id="layout3">All Lab Information</a></li>

                                        </ul>

                                    </li>
                                    <li><a href="#"><i class="icon-list-unordered"></i> <span>Software Managment</span></a></li>
                                    <li><a href="#"><i class="icon-list-unordered"></i> <span>Assign Course</span></a></li>


                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
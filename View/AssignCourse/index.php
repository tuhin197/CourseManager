<?php
include_once("../../vendor/autoload.php");
use App\AssignCourse\AssignCourse;
$obj = new AssignCourse();
$showData = $obj->index();
//echo '<pre>';
//print_r($showData);
//die();
include 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
  

                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Basic datatable</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="">

                                <!-- Main -->
                                <li><a href="add-new-lab.php" id="layout2">Add new lab</a></li>
                                <li><a href="alllabinfo.php" id="layout3">All Lab Information</a></li>
                                <li><form action="">
                                        First name: <input type="text" id="txt1" onkeyup="showHint(this.value)">
                                    </form>

                                    <p>Suggestions: <span id="txtHint"></span></p></li>
                            </ul>
                            <script>
                                function showHint(str) {
                                    var xhttp;
                                    if (str.length == 0) {
                                        document.getElementById("txtHint").innerHTML = "";
                                        return;
                                    }
                                    xhttp = new XMLHttpRequest();
                                    xhttp.onreadystatechange = function () {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                            document.getElementById("txtHint").innerHTML = xhttp.responseText;
                                        }
                                    };
                                    xhttp.open("GET", "livesearch.php?id=" + str, true);
                                    xhttp.send();
                                }
                            </script>
                            <div class="panel-body">

                            </div>

                            <table class="table datatable-basic">
                                <thead>

                                    <tr>
                                        <th>ID</th>
                                        <th>Course title</th>
                                        <th>Batch NO</th>
                                        <th>Lab NO</th>
                                        <th>Lead Trainer</th>
                                        <th colspan="3" class="text-center">Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $serial = 1;
                                    if (!empty($showData)) {
                                        foreach ($showData as $one) {
                                            ?>
                                            <tr>
                                                <td><?php echo $serial++; ?></td>
                                                <td><?php echo $one['course_id'] ?></a></td>
                                                <td><?php echo $one['batch_no'] ?></td>
                                                <td><?php echo $one['lab_id'] ?></td>
                                                <td><?php echo $one['lead_trainer'] ?></td>
                                                <td><a href="edit.php?id=$one['id']"><i class="fa fa-remove"></i></a></td>
                                                <td><a href="view.php?id=<?php echo $one['id'] ?>">show</a></td>
                                                <td><a href="edit.php?id=$one['id']">Edit</a></td>

                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        echo "No data";
                                    }
                                    ?>


                                </tbody>
                            </table>
                        </div>
                        <!-- /basic datatable -->






                        <!-- Footer -->
                        <div class="footer text-muted">
                            &copy; 2015. <a href="#">Tuhin</a> by <a href="#" target="_blank">Nai</a>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>

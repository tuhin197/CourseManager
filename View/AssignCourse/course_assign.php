<?php
include_once("../../vendor/autoload.php");

use App\AssignCourse\AssignCourse;

$obj = new AssignCourse();

$showData = $obj->index();
$getlabinfo = $obj->getlabInfo();
$live = $obj->livedata();
?>
<!DOCTYPE html>
<html lang="en">
   <?php
   include 'header.php';
   ?>


                    <!-- Content area -->
                    <div class="content">
                        <div class="row">
                            <form class="form-horizontal" action="store.php" method="post">
                                <fieldset class="content">
                                    <legend class="text-bold">Basic inputs</legend>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Course Title:</label>
                                            <div class="col-md-9" id="output1">
                                                <select id="" onchange="showUser(this.value)" class="form-control" name="course_id">
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    if (!empty($live)) {
                                                        foreach ($live as $rs) {
                                                            ?>
                                                            <option value="<?php echo $rs['id']; ?>"><?php
                                                                if (!empty($rs['title'])) {
                                                                    echo $rs['title'];
                                                                }
                                                                ?></option>
    <?php }
}
?>                      
                                                </select>
                                                <script>
function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","livedata.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Batch No:</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" placeholder="Enter Batch Number" name="batch_no">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lead Trainer:</label>
                                            <div class="col-md-9">
                                                <select id="txtHint" onchange="showUser1(this.value)" class="form-control" name="lead_trainer">
                                                    <option>Please Select</option>
                                                    
                                                </select>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                    function showUser1(str) {
    if (str == "") {
        document.getElementById("txtHint1").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint1").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","getassttrainer.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>
                     

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Ass. Trainer:</label>
                                            <div class="col-md-9">
                                                <select id="txtHint1" onchange="showUser2(this.value)" class="form-control" name="asst_trainer">
                                                    <option>Please Select</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     <script>
function showUser2(str) {
    if (str == "") {
        document.getElementById("txtHint2").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint2").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","getlabtrainer.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>

                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lab Asst:</label>
                                            <div class="col-md-9">
                                                <select id="txtHint2"  class="form-control" name="lab_asst">
                                                    <option>Please Select</option>
                                                    
                                                </select>
                                         
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lab No:</label>
                                            <div class="col-md-9">
                                                <select id="os" onchange="" class="form-control" name="lab_id">
                                                    <?php foreach ($getlabinfo as $r) { ?>
                                                        <option value="<?php echo $r['id']; ?>"><?php echo $r['lab_no']; ?></option>
                                                    <?php } ?>     
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Start Date:</label>
                                            <div class="col-md-9">
                                                <input type="text"  id="datepicker" class="form-control"  name="start_date" value="" placeholder="Select Date">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Ending Date:</label>
                                            <div class="col-md-9">
                                                <input type="text"  id="datepickerend" class="form-control"  name="ending_date" value="" placeholder="Select Date">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Start Time:</label>
                                            <div class="col-md-3">
                                                <select name="start_time" class="form-control">
                                                    <<option value="">Hour</option>
                                                    <option value="1">1</option>
                                                      <option value="2">2</option>
                                                        <option value="3">3</option>
                                                          <option value="4">4</option>
                                                            <option value="5">5</option>
                                                              <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                      <option value="10">10</option>
                                                                      <option value="11">11</option>
                                                                      <option value="12">12</option>
                                                </select>

                                            </div>
                                            <div class="col-md-3">
                                                <select name="start_time" class="form-control">
                                                    <option value="">Minite</option>
                                                    <option value="00">00</option>
                                                    <option value="10">10</option>
                                                      <option value="20">20</option>
                                                        <option value="30">30</option>
                                                          <option value="40">40</option>
                                                            <option value="50">50</option>
                                                              <option value="60">60</option>
                                                                
                                                </select>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Ending Time:</label>
                                           <div class="col-md-3">
                                                <select name="ending_time" class="form-control">
                                                    <<option value="">Hour</option>
                                                    <option value="1">1</option>
                                                      <option value="2">2</option>
                                                        <option value="3">3</option>
                                                          <option value="4">4</option>
                                                            <option value="5">5</option>
                                                              <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                      <option value="10">10</option>
                                                                      <option value="11">11</option>
                                                                      <option value="12">12</option>
                                                </select>

                                            </div>
                                            <div class="col-md-3">
                                                <select name="ending_time" class="form-control">
                                                    <option value="">Minite</option>
                                                    <option value="00">00</option>
                                                    <option value="10">10</option>
                                                      <option value="20">20</option>
                                                        <option value="30">30</option>
                                                          <option value="40">40</option>
                                                            <option value="50">50</option>
                                                              <option value="60">60</option>
                                                                
                                                </select>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Day:</label>
                                            <div class="col-md-9">
                                                <select id="os" onchange="" class="form-control" name="day">
                                                    
                                                    <option value="Sat,Mon,Wed">Saturday,Monday,Wednesday</option>
                                                    <option value="Sun,Tue,Thu">Sunday,Tuesday,Thursday</option>
                                                    <option value="Friday">Friday</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                   
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary col-md-2 pull-right">Assign New course <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </fieldset>


                            </form>
                        </div>
                    </div>




                    <!-- Footer -->
                    <div class="footer text-muted">
                        &copy; 2015. <a href="#">Tuhin</a> by <a href="#" target="_blank">Nai</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
    <script>

        var picker = new Pikaday(
                {
                    field: document.getElementById('datepicker'),
                    firstDay: 1,
                    minDate: new Date(),
                    maxDate: new Date(2020, 12, 31),
                    yearRange: [2000, 2020]
                });

    </script>
    <script>

        var picker = new Pikaday(
                {
                    field: document.getElementById('datepickerend'),
                    firstDay: 1,
                    minDate: new Date(),
                    maxDate: new Date(2020, 12, 31),
                    yearRange: [2000, 2020]
                });


    </script>
    <script>


        var dd = new pi(
                {
                    field: document.getElementById('time'),
                    start: '9:00 am', // what's the first available hour
                    end: '6:00 pm', // what's the last avaliable hour
                    top_hour: 12
                });

    </script>
</body>
</html>


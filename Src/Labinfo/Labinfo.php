<?php

namespace App\Labinfo;

use PDO;

class Labinfo {

    public $conn = '';
    public $id = '';
    public $users_id = '';
    public $lab_no = '';
    public $seat_capacity = '';
    public $projecht_resu = '';
    public $ac_status = '';
    public $pc_conf = '';
    public $os = '';
    public $trainer_pc_conf = '';
    public $table_capa = '';
    public $internet_speed = '';
    public $course_id = '';
    public $dbuser = 'root';
    public $dbpass = '';
    public $data = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=course_manger', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '') {
       
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['course_id'])) {
            $this->course_id = $data['course_id'];
        }
        if (!empty($data['lab_no'])) {
            $this->lab_no = $data['lab_no'];
        } else {
            $_SESSION['lab_requir'] = "Lab Number  Required";
        }
        if (!empty($data['seat_capacity'])) {
            $this->seat_capacity = $data['seat_capacity'];
        } else {
            $_SESSION['seat_requir'] = "Seat Capacity Required";
        }
        if (!empty($data['projector_resolution'])) {
            $this->projecht_resu = $data['projector_resolution'];
        } else {
            $_SESSION['projector_requir'] = "Projector Resolution Required";
        }
        if (!empty($data['ac_status'])) {
            $this->ac_status = $data['ac_status'];
        } else {
            $_SESSION['ac_requir'] = "Select Ac Status";
        }
        if (!empty($data['pc_configuration'])) {
            $this->pc_conf = $data['pc_configuration'];
        } else {
            $_SESSION['pc_requir'] = "PC Configuration Required";
        }
        if (!empty($data['os'])) {
            $this->os = $data['os'];
        } else {
            $_SESSION['os_requir'] = "Select Operating System";
        }
        if (!empty($data['trainer_pc'])) {
            $this->trainer_pc_conf = $data['trainer_pc'];
        } else {
            $_SESSION['trainer_pc_requir'] = "Trainer Pc Config required";
        }
        if (!empty($data['table_capacity'])) {
            $this->table_capa = $data['table_capacity'];
        } else {
            $_SESSION['table_requir'] = "Table Capacity Required";
        }
        if (!empty($data['internet_speed'])) {
            $this->internet_speed = $data['internet_speed'];
        } else {
            $_SESSION['internet_requir'] = "Internet Speed Required";
        }
        $_SESSION['requir_data'] = $data;
        return $this;
    }

    public function store() {
        try {

            $query = "INSERT INTO labinfo(id, course_id, lab_no, seat_capacity, projector_resolution, ac_status, pc_configuration, os, trainer_pc, table_capacity, internet_speed)
                  VALUES(:i, :ci, :ln, :sc, :pr, :ac, :pc, :o, :tpc, :tc, :is )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':i' => NULL,
                ':ci' => 10,
                ':ln' => $this->lab_no,
                ':sc' => $this->seat_capacity,
                ':pr' => $this->projecht_resu,
                ':ac' => $this->ac_status,
                ':pc' => $this->pc_conf,
                ':o' => $this->os,
                ':tpc' => $this->trainer_pc_conf,
                ':tc' => $this->table_capa,
                ':is' => $this->internet_speed
            ));
             $_SESSION['success_mess'] = "Data Save Successfully";
            header('location:add-new-lab.php');
            
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    public function index() {
        try {
            $query = "SELECT * FROM `labinfo` ";
            $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show() {

        $query = "SELECT * FROM labinfo WHERE id=$this->id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function delete() {
        $query = "DELETE FROM `labinfo` WHERE  id=$this->id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        header('location:alllabinfo.php');
        return $this;
    }

    public function update() {
        $dat = date('Y-m-d h:m:s');
        try {
            $query = "UPDATE profiles SET fullname='$this->fullname', fathers_name='$this->fathers_name', mothers_name='$this->mothers_name', gender='$this->gender', birthday='$this->birthday', mobile='$this->mobile', occupation='$this->occupation', education='$this->education', religion='$this->religion', marital='$this->marital', jobstatus='$this->jobstatus', nationality='$this->nationality', interested='$this->interested', nid='$this->nid', passport='$this->passport', skill='$this->skill', language='$this->language', blood='$this->blood', faxnumber='$this->faxnumber', height='$this->height', address='$this->address', web_address='$this->web_address', profile_pic='$this->profile_pic', others='$this->others', created=date('Y-m-d h:m:s'), modified=date('Y-m-d h:m:s'), deleated='$this->deleated', bio='$this->bio' WHERE users_id=$this->users_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            header('location:profileupdate.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search() {

        $query = "SELECT * FROM labinfo WHERE id=$this->id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
        return $this;
    }

}

<?php
namespace App\AssignCourse;

use PDO;

class AssignCourse {
     public $conn = '';
    public $id = '';                    
    public $course_id = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_id = '';
    public $start_date = '';
    public $ending_date = '';
    public $start_time = '';
      public $ending_time = '';
    public $day = '';
    public $is_running = '';
    public $assing_by = '';
    public $created = '';
    public $modified = '';
    public $delete = '';
    public $dbuser ='root';
    public $dbpass = '';
    public $data = '';
    public $livedata = '';
    public $getlab = '';
    public $gettrain = '';




    public function __construct()
    {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=course_manger', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '')
    {
        print_r($data);
        die();
        if (!empty($data['id'])) {      
            $this->id = $data['id'];
        }
          if (!empty($data['course_id'])) {
            $this->course_id = $data['course_id'];
        }
          if (!empty($data['batch_no'])) {
            $this->batch_no = $data['batch_no'];
        }
          if (!empty($data['lead_trainer'])) {
            $this->lead_trainer = $data['lead_trainer'];
        }
          if (!empty($data['asst_trainer'])) {
            $this->asst_trainer = $data['asst_trainer'];
        }
          if (!empty($data['lab_asst'])) {
            $this->lab_asst = $data['lab_asst'];
        }
          if (!empty($data['lab_id'])) {
            $this->lab_id = $data['lab_id'];
        }
          if (!empty($data['start_date'])) {
            $this->start_date = $data['start_date'];
        }
        if (!empty($data['ending_date'])) {
            $this->ending_date = $data['ending_date'];
        }
          if (!empty($data['start_time'])) {
            $this->start_time = $data['start_time'];
        }
          if (!empty($data['ending_time'])) {
            $this->ending_time = $data['ending_time'];
        }
          if (!empty($data['day'])) {
            $this->day = $data['day'];
        }
        
        return $this;
    }
    public function storecourse() {
        try {

            $query = "INSERT INTO `course_manger`.`course_trainer_lab_mapping` (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`) VALUES (NULL, '$this->course_id', '$this->batch_no', '$this->lead_trainer', '$this->asst_trainer', '$this->lab_asst', '$this->lab_id', '$this->start_date', '$this->ending_date','$this->start_time' , '$this->ending_time', '$this->day', '2', 'tuhin', '2016-08-23 00:00:00', '2016-08-23 00:00:00', '2016-08-23 00:00:00')";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
           
             $_SESSION['success_mess'] = "Data Save Successfully";
            header('location:course_assign.php');
            
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }
    public function leftjoin(){
       $query= "SELECT * FROM `trainer` left join courses on labinfo.course_id = courses.id WHERE labinfo.`id`=10";
                 $stmt = $this->conn->prepare($query);
        $stmt->execute();
    }
     public function index() {
        try {
            $query = "SELECT * FROM `course_trainer_lab_mapping` ";
            $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show() {

        $query = "SELECT * FROM labinfo WHERE id=$this->id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function delete() {
        $query = "DELETE FROM `labinfo` WHERE  id=$this->id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        header('location:alllabinfo.php');
        return $this;
    }

    public function update() {
        $dat = date('Y-m-d h:m:s');
        try {
            $query = "UPDATE profiles SET fullname='$this->fullname', fathers_name='$this->fathers_name', mothers_name='$this->mothers_name', gender='$this->gender', birthday='$this->birthday', mobile='$this->mobile', occupation='$this->occupation', education='$this->education', religion='$this->religion', marital='$this->marital', jobstatus='$this->jobstatus', nationality='$this->nationality', interested='$this->interested', nid='$this->nid', passport='$this->passport', skill='$this->skill', language='$this->language', blood='$this->blood', faxnumber='$this->faxnumber', height='$this->height', address='$this->address', web_address='$this->web_address', profile_pic='$this->profile_pic', others='$this->others', created=date('Y-m-d h:m:s'), modified=date('Y-m-d h:m:s'), deleated='$this->deleated', bio='$this->bio' WHERE users_id=$this->users_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            header('location:profileupdate.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search() {

        $query = "SELECT * FROM labinfo WHERE id=$this->id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
        return $this;
    }
    public function livedata(){
        try {
        $query = "SELECT id, title FROM `courses`";
         $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->livedata[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->livedata;
        return $this;
    }
     public function getlabInfo(){
        try {
        $query = "SELECT id, lab_no FROM `labinfo`";
         $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->getlab[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->getlab;
        return $this;
    }
    public function gettrainerinfo(){
        try {
        $query = "SELECT course_id, trainer_status FROM `trainer`";
         $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->gettrain[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->gettrain;
        return $this;
    }
    public function getlabas(){
        try {
        $query = "SELECT course_id, trainer_status FROM `trainer`";
         $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->gettrain[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->gettrain;
        return $this;
    }
     public function getlabas1(){
        try {
        $query = "SELECT course_id, trainer_status FROM `trainer`";
         $q = $this->conn->query($query);
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->gettrain[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->gettrain;
        return $this;
    }
    
    

    
}

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2016 at 08:42 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `course_manger`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
`id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE IF NOT EXISTS `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` int(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` date NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `is_running` int(11) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_trainer_lab_mapping`
--

INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`) VALUES
(2, 22, 'asd', 'sad', 'sad', 'asdf', 0, 'adf', 'asdf', '0000-00-00', 'dsf', 'adsf', 255, 'ads', '2016-08-18 00:00:00', '2016-08-18 00:00:00', '2016-08-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE IF NOT EXISTS `installed_softwares` (
`id` int(11) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE IF NOT EXISTS `labinfo` (
`id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lab_no` varchar(255) NOT NULL,
  `seat_capacity` varchar(255) NOT NULL,
  `projector_resolution` varchar(255) NOT NULL,
  `ac_status` varchar(255) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc` varchar(255) NOT NULL,
  `table_capacity` varchar(255) NOT NULL,
  `internet_speed` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc`, `table_capacity`, `internet_speed`) VALUES
(11, 10, '02', '03', '1200*100', 'yes', 'Dual configurartion', 'linux', '', '333', '512'),
(12, 10, '3', 'd', 'asdf', 'yes', 'ad', 'linux', '', 'ads', 'dsa'),
(13, 10, '323', '33', 'adsf', 'yes', 'sdaf', 'linux', '', 'ads', 'adsf'),
(14, 10, '12', '21', '21212', 'yes', '122112', 'linux', '122121', 'sadfsad', 'asd'),
(15, 10, '102', '30', '1260', 'yes', 'dzsfsd', 'linux', 'dsfdsf', 'dsfdsf', 'asd'),
(16, 10, '323', '30', '1200x100', 'yes', 'something goes there', 'window', 'mac', '15', '2mbps');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE IF NOT EXISTS `trainers` (
`id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `trainer_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
